#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import redis

# ！！！set 操作是无序的，每次打印结果的顺序可能不一样
if __name__ == '__main__':
    r = redis.Redis(host="127.0.0.1", port=6379, password="123456")
    print('sadd 利用set不重复的机制，添加重复元素的时候，不会添加进去')
    print(r.sadd("1", 1))  # 输出的结果是1
    print(r.sadd("1", 2))  # 输出的结果是1
    print(r.sadd("1", 2))  # 因为2已经存在，不能再次添加，所以输出的结果是0
    print(r.sadd("1", 3, 4))  # 输出的结果是2

    print(r.sadd("2", 1))  # 输出的结果是1
    print(r.sadd("2", 2))  # 输出的结果是1
    print(r.sadd("2", 2))  # 因为2已经存在，不能再次添加，所以输出的结果是0
    print(r.sadd("2", 3, 4, 5))  # 输出的结果是2

    # r.sinter()   求多个键名对应的集合的交集
    print(r.sinter("1", "2"))  # 输出的结果是set(['1', '3', '2', '4'])

    print()
    print('scard 列出给定名称集合的大小')
    print(r.sadd("3", 1))  # 输出的结果是1
    print(r.sadd("3", 2, 3, 4, 5))  # 输出的结果是1
    print(r.scard("2"))  # 输出的结果是5

    print()
    print('sdiff() 求多个键名对应的集合的差集，需要注意求差集的顺序，谁在前差集就是谁的真子集')
    print(r.sadd("31", 1, 2, 3, 4, 5, 6))  # 输出的结果是6
    print(r.sadd("32", 4, 5, 6, 7, 8, 9))  # 输出的结果是6
    print(r.sdiff(31, 32))  # 输出的结果是set(['1', '3', '2'])
    print(r.sdiff(32, 31))  # 输出的结果是set(['9', '8', '7'])
    print(r.sdiff(31, 31))  # 输出的结果是set([])
    print(r.sdiff("31", "32"))

    print()
    print('Sdiffstore干了俩事，求差集，然后将差集存到dest集合里面，目标集合存在的话就覆盖')
    print(r.sadd("41", 1, 2, 3, 4, 5, 6))  # 输出的结果是6
    print(r.sadd("42", 4, 5, 6, 7, 8, 9))  # 输出的结果是6
    print(r.sadd("43", 0))  # 输出的结果是1
    print(r.sdiffstore(dest="43", keys=("41", "42")))  # 输出的结果是3
    print(r.sinter("43"))  # 输出的结果是 set(['1', '3', '2'])

    print()
    print('sinter 输出两个键名对应的集合的交集，该交集同是两个集合的真子集')
    print(r.sadd("51", 3, 4, 5, 6))  # 输出的结果是4
    print(r.sadd("52", 1, 2, 3, 4))  # 输出的结果是4
    print(r.sinter(51, 52))  # 输出的结果是set(['3', '4'])
    print(r.sadd("53", 1, 2, 3, 4, 5, 6))  # 输出的结果是6
    print(r.sadd("54", 3, 4, 5, 6, 7, 8, 9))  # 输出的结果是7
    print(r.sinter(53, 54))  # 输出的结果是set(['3', '5', '4', '6'])
    print(r.sinter(53, 56))  # 输出的结果是set([])

    print()
    print('Sinterstore 也是干俩事，求交集，然后存储到dest集合中，如果dest集合存在则覆盖，不存在则新建')
    print(r.sadd("61", 2, 3, 4, 5, 6))  # 输出的结果是4
    print(r.sadd("62", 1, 2, 3, 4))  # 输出的结果是4
    print(r.sadd("63", 0))  # 输出的结果是1
    print(r.sinterstore(63, 61, 62))  # 输出的结果是2
    print(r.sinter(63))  # 输出的结果是set(['3', '4'])

    print()
    print('Sismember 判断集合里面是否包含对应的元素')
    print(r.sadd("71", 1, 2, 3, 4, 5, 6))  # 输出的结果是6
    print(r.sismember("71", 1))  # 输出的结果是True
    print(r.sismember("71", 2))  # 输出的结果是True
    print(r.sismember("71", 7))  # 输出的结果是False
    print(r.sismember("71", 8))  # 输出的结果是False

    print()
    print('Smembers 命令返回集合中的所有的成员。 不存在的集合 key 被视为空集合。')
    print(r.sadd("81", 1, 2, 3, 4, 5, 6))  # 输出的结果是6
    print(r.smembers(81))  # 输出的结果是set(['1', '3', '2', '5', '4', '6'])
    print(r.smembers(82))  # 输出的结果是set([])

    print()
    print('Smove  原子性操作，将集合中的元素移动到指定集合中去，指定集合存在则覆盖，不存在则添加。源集合存在则移动，不存在则不会移动')
    print(r.sadd("91", 1, 2, ))  # 输出的结果是2
    print(r.sadd("92", 1, 3, 4, 1))  # 输出的结果是2
    print(r.smove(91, 92, 1))  # 把91中的1移动到92中去，输出的结果是True
    print(r.smembers("91"))  # 输出的结果是set(['2'])
    print(r.smembers("92"))  # 输出的结果是set(['1', '3', '4'])
    print(r.smove(91, 92, 5))  # 91不存在5，输出的结果是False
    print(r.smembers("91"))  # 输出的结果是set(['2'])
    print(r.smembers("92"))  # 输出的结果是set(['1', '3', '4'])

    print()
    print('spop 从指定集合中移除一个元素，随机的！规则是set的无序机制')
    print(r.sadd("10", 1, 2, 3, 4, 5, 6))  # 输出的结果是6
    print(r.spop("10"))  # 输出的结果是3
    print(r.smembers("10"))  # 输出的结果是set(['1', '2', '5', '4', '6'])
    print(r.spop("10"))  # 输出的结果是1
    print(r.smembers("10"))  # 输出的结果是set(['2', '5', '4', '6'])

    print()
    print('srandmember 获取集合随机元素，新增：可以指定获取N个，当N大于等于集合大小时，会按照升序排列，小于时是随机顺序排列')
    print(r.sadd("11", 1, 2, 3, 4, 5, 6))  # 输出的结果是6
    print(r.srandmember(11))  # 输出的结果是4
    print(r.smembers(11))  # 输出的结果是set(['1', '3', '2', '5', '4', '6'])
    print(r.srandmember(11, 2))  # 输出的结果是['6', '3', '1']
    print(r.srandmember(11, 6))  # 输出的结果是['6', '3', '1']
    print(r.smembers(11))  # 输出的结果是set(['1', '3', '2', '5', '4', '6'])

    print()
    # s remove
    print('Srem 命令用于移除集合中的一个或多个成员元素，不存在的成员元素会被忽略。返回删除元素的个数，被成功移除的元素算数量，被忽略的不算。')
    print(r.sadd("12", 1, 2, 3, 4, 5, 6, 7))  # 输出的结果是7
    print(r.srem("12", 4, 5, 6, 10))  # 输出的结果是1
    print(r.smembers("12"))  # 输出的结果是set(['3', '2', '5', '4', '7', '6'])
    print(r.srem("12", 8))  # 输出的结果是0
    print(r.smembers("12"))  # 输出的结果是set(['3', '2', '5', '4', '7', '6'])

    print()
    print('Sunion 命令返回给定集合的并集。不存在key的集合被视为空集，和空集求并集等于集合本身')
    print(r.sadd("131", 1, 2, 3, 4, 5, 6, 7))  # 输出的结果是7
    print(r.sadd("132", 0, 1, 2, 7, 8, 9))  # 输出的结果是6
    print(r.sadd("133", 0))  # 插入133
    print(r.srem(133, 0))  # 删除133元素
    print(r.sunion(131, 132))  # 输出的结果是set(['1', '0', '3', '2', '5', '4', '7', '6', '9', '8'])
    print(r.sunion(131, 133))
    print(r.sunion(131, 134))  # 输出的结果是set(['1', '3', '2', '5', '4', '7', '6'])

    print()
    print('Sunionstore 命令将给定集合的并集存储在指定的集合 destination 中。与不存在或者为空求并集则相当于拷贝源集合到目标集合。另外，当目标集合已经存在元素，则会忽略')
    print(r.sadd("141", 1, 2, 3, 4, 5, 6, 7))  # 输出的结果是7
    print(r.sadd("142", 0, 1, 2, 3, 4))  # 输出的结果是5
    print(r.sunionstore(143, 141, 142))  # 输出的结果是8
    print(r.smembers(143))  # 输出的结果是set(['1', '0', '3', '2', '5', '4', '7', '6'])
    print(r.sunionstore(144, 141, 145))
    print(r.smembers(144))

    print()
    print('Sscan 命令用于迭代集合键中的元素。类似关系型数据库的查询')
    print(r.sadd("151", 1, 2, 3, 4, 5, 6, 7, 11, 111, 1111, 11111))  # 输出的结果是7
    print(r.sscan(151, cursor=1, match='1*', count=10))  # 输出的结果是 (0L, ['1'])
