#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# redis 事务原子操作一波
import time

import redis
from redis import ResponseError

if __name__ == '__main__':
    r = redis.Redis(host = "127.0.0.1", port = 6379, password = "123456")
    pip = r.pipeline()
    pip.set('ssss001', 'abbb')
    pip.set('ssss002', 'bbbba')
    print(pip.execute())
    print(r.get('ssss001').decode('utf-8'))
    print(r.get('ssss002').decode('utf-8'))

    # 添加初值的时候就带上有效时间
    print(r.set('s12', 12, ex = 1))
    # 等待2秒
    # time.sleep(2)
    print(r.exists('s12'))
    # 再次设置有效时间，键值对已经过期了，设置无效
    print(r.expire('s12', 20))

    print()
    print('Expireat 命令是以unix时间戳作为到期时间的，10位')
    print('PEXPIREAT 命令是以unix 毫秒级时间戳作为到期时间，13位')
    print(r.set('s3', '2222222'))
    # 设置秒级时间戳作为到期时间
    print(r.expireat('s3', int(time.time() + 2)))
    # 设置毫秒级时间戳作为到期时间
    print(r.pexpireat('s3', int(round(time.time() * 1000)) + 2000))

    print()
    print('keys 匹配以*为匹配规则，类似前置匹配和后置匹配')
    print(r.set('s14', 555555))
    print(r.keys('*4*'))

    print()
    print('move 将一个键值对，移动到另外一个db分库中去，目标库若已存在则操作失败返回false。若源库不存在，则返回false')
    print(r.set('s5', 55555))
    print(r.move('s5', 4))
    print(r.move('s51', 3))

    print()
    print('persist 命令是将一个键值对的过期取消，如果本身就是永久保存的，则操作失败返回false')
    print(r.set('s6', 1111, 1))
    print(r.persist('s6'))
    print(r.persist('s6'))

    print()
    print('ttl 和pttl  都是用于返回键值对的有效剩余时间，tll单位是秒，pttl单位是毫秒。永久返回-1 ，不存在返回-2')
    print(r.set('s7', '7777', 1))
    print(r.ttl('s7'))
    print(r.pttl('s7'))
    print(r.ttl('s77'))
    print(r.pttl('s77'))
    print(r.set('ss7', 's77'))
    print(r.ttl('ss7'))
    print(r.pttl('ss7'))

    print()
    print('randomkey 命令是从指定库中获得一个随机key')
    print(r.randomkey().decode('utf-8'))
    print('type 是返回键值对的值类型')
    print(r.type(r.randomkey().decode('utf-8')).decode('utf-8'))

    print()
    print('rename 命令是修改键值对的key名称')
    print(r.set('s8', 8888))
    print(r.rename('s8', 's88'))
    print(r.get('s8'))
    print(r.get('s88').decode('utf-8'))
    print('当重命名key不存在时候可以使用 renamenx 会抛出异常，异常信息是：no such key')
    print('此时需要捕获异常并处理')
    try:
        print(r.renamenx('s8', "s888"))
    except ResponseError as e:
        print(e)
