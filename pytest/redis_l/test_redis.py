#!/usr/bin/env python3
# -*- coding: utf-8 -*
# https://www.cnblogs.com/john-xiong/p/12089103.html
import redis

if __name__ == '__main__':
    r = redis.Redis(host = 'localhost', port = 6379, db = 0, password = '123456')
    print(r.set('aaa', 'bbb'))
    print(r.get('aaa'))

    # 存在则修改，不存在则返回None且不做修改操作
    print(r.getset('aaa1', 'ccc'))

    print(r.set('a1', 'a1', ex = 2))
    print(r.ttl('a1'))  # 获取对象存活剩余时间
    print(r.get('a1'))
    print(r.set('a2', 'a2'))
    print(r.set('a3', 'a3'))
    # 以List返回，可以传入list或者tuple，批量获取对象，存在即返回元素，不存在则返回None
    print(r.mget(keys = ('a1', 'a2', 'a3', 'a4')))

    # 不存在则添加，存在则返回false，运行两次能看到效果
    print(r.setnx('b1', 'bbbb'))

    # 添加对象并设置超时时间,不存在则创建，存在则修改
    print(r.setex('name', 1, 'James'))

    # redis的字符串操作，类似实际的字符串操作，字符串范围内替换，范围外增加
    print(r.set('name', 'Hello'))
    print(r.setrange('name', 7, 'World'))
    print(len(r.get('name').decode()))
    # 批量添加或修改对象，传入字典
    print(r.mset({'a1': 'a1', 'a2': 'a2'}))
    # 不存在则创建，不存在返回false忽略此次操作
    print(r.msetnx({'a1': 'Smith', 'a3': 'Curry'}))

    # 数字的加减操作，必须确定存的是数字，否则会抛异常
    print(r.set('a1', 100))
    print(r.incr('a1', 2))
    print(r.decr('a1', 3))
    print(r.info())
    print()
    print(r.set('a1', '111111'))
    print(r.setrange('a1', 20, '22222222'))
    print(r.get('a1').decode('utf-8'))
    print(len((r.get('a1').decode('utf-8').strip())))
