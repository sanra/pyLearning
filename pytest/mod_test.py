# !/usr/bin/python3
# -*-coding:utf-8-*-
# Author: sanra
# CreatDate: 2020/11/16 20:07
# Description: 取模和取余计算


# 取模，Python中可直接用%，计算模，r = a % b
# // 表示可整除部分的数据 例如 12、13、14 // 3 都等于4
def mod(a, b):
    c = a // b
    print('取模 c ：%s' % str(c))
    r = a - c * b
    print('取模 r ：%s' % str(r))
    return r


# 取余
# 取余就是整除完剩下的那部分，比如 10 rem 3 =1
def rem(a, b):
    c = int(a / b)
    print('取余 c ：%s' % str(c))
    r = a - c * b
    print('取余 r ：%s' % str(r))
    return r


if __name__ == '__main__':
    mod(10, 3)
    rem(10, 3)

    print(12 // 3)
