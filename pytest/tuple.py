#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 元组非常好用，后面用处比较多，不会了需要返回来看看这个
# 固定参数对和非固定元组可以互转，相当于数据装箱拆箱操作
# *号参数 表示元组
# **号参数表示字典

def tuple_return():
    return 1, 2, 3


if __name__ == '__main__':
    a = 1, 10, 100, '11111', [1, 2, 3, 4]
    print(type(a))

    i, j, k, o, x = a
    print(type(i))
    print(type(j))
    print(type(k))
    print(type(o))
    print(type(x))
    print("**************tuple**************")
    print()
    x = a, b, *c = range(1, 10)
    print(type(x))
    print(type(a))
    print(type(b))
    print(type(c))
    print(a, b, c)
    # if isinstance(c, list):
    #     for x in c:
    #         print(x)
    a, b, c = [1, 10, 100]
    print(a, b, c)
    a, *b, c, d = 'hello'
    print(a, b, c, d)
    print(type(b))
    print()

    a = 1, 10, 100, 1000, 10000, 1000000
    i, *j, k = a
    print(i, j, k)  # 1 10 [100, 1000]

    print(type(k))
    i, *j, k = a
    print(i, j, k)  # 1 [10, 100] 1000
    *i, j, k = a
    print(i, j, k)  # [1, 10] 100 1000
    *i, j = a
    print(i, j)  # [1, 10, 100] 1000
    i, *j = a
    print(i, j)  # 1 [10, 100, 1000]
    i, j, k, *o = a
    print(i, j, k, o)  # 1 10 100 [1000]
    i, j, k, o, *m = a
    print(i, j, k, o, m)  # 1 10 100 1000 []

    x = tuple_return()
    print(type(x))
    print(x)

    # 用元组接的时候发现不够的时候，返回一个空元组
    x = 1, 10, 100
    a, b, c, *d = x
    print(a, b, c, d)
