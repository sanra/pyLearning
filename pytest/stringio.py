#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from io import StringIO, BytesIO

if __name__ == '__main__':
    f = StringIO('Hello!\nHi!\nGoodbye!')
    while True:
        s = f.readline()
        if s == '':
            break
        print(s.strip())

    b = BytesIO()
    b.write('我'.encode('utf-8'))
    print(b.getvalue())
