#!/usr/bin/env python3
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    # 头尾带单引号的hello, world!
    s1 = '\'hello, world!\''
    print(s1)
    # 头尾带反斜杠的hello, world!
    s2 = '\\hello, world!\\'
    print(s2)

    # 字符串s1中\t是制表符，\n是换行符
    s1 = '\time up \now'
    print(s1)
    # 字符串s2中没有转义字符，每个字符都是原始含义
    s2 = r'\time up \now'
    print(s2)

    # 这里格外提出来的，\x00 是个很奇怪的字符，存在在redis里面，但输出自负不显示
    sb3 = b'111111\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0022222222'
    s3 = sb3.decode(encoding='UTF-8').strip()
    print('strip 方法执行后结果：%s' % s3)
    print(len(s3))
    s3 = s3.replace(b'\x00'.decode('utf-8'), '')
    print('replace 方法执行后结果：%s' % s3)
    print(len(s3))

    print()
    print(repr(10000))

    print()
    abb = '1112336688'
    print(eval('abb'))
    print(type(eval('abb')))

    fruits = ['banana0', 'apple0', 'mango0','banana1', 'apple1', 'mango1','banana2', 'apple2', 'mango2','banana3', 'apple3', 'mango3']
    # 开始,结束,步长
    for index in range(0, 10, 2):  # range（0， 5） 等价于 range(0, 5, 1)
        print(index)
        print(u'当前水果 :', fruits[index])

