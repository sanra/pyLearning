#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import pickle


def pickle_2_byte():
    d = dict(name='bob', age=20, score=80)
    print(d)
    print(pickle.dumps(d))

    d = ('1111', 2222)
    print(d[0])
    print(d[1])
    print(d)


class Student(object):
    def __init__(self, name, age, score):
        self.name = name
        self.age = age
        self.score = score


def student2dict(std):
    return {
        'name': std.name,
        'age': std.age,
        'score': std.score
    }


def json_test():
    s = Student('Bob', 20, 88)
    print(json.dumps(s, default=student2dict))


if __name__ == '__main__':
    pickle_2_byte()
    json_test()
