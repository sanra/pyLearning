#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from tornado.tcpserver import TCPServer
import tornado
from tornado.netutil import bind_sockets



def aaa():
    sockets = bind_sockets(8888)
    tornado.process.fork_processes(0)
    server = TCPServer()
    server.add_sockets(sockets)
    from tornado.ioloop import IOLoop
    IOLoop.current().start()


if __name__ == '__main__':
    aaa()
