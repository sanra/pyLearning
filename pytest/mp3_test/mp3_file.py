# !/usr/bin/python3
# -*-coding:utf-8-*-
# Author: sanra
# CreatDate: 2020/11/17 16:37
# Description: 读取mp3 内容
import os
from pymediainfo import MediaInfo

if __name__ == '__main__':
    curr_path = os.getcwd() + '\\'
    # 先从本地获取 mp3 的 bytestring 作为数据样本
    filename = curr_path + "b.mp3"
    media_info = MediaInfo.parse(filename)
    data = media_info.to_json()
    print(data)

    # fp = open(filename, 'rb')
    # data = fp.read()
    # fp.close()
    # # 读取
    # aud = io.BytesIO(data)
    # print(aud.getvalue())
    #
    # mp3_sound = AudioSegment.from_mp3(file=filename)
    # raw_data = mp3_sound.raw_data()
    #
    # # 写入到文件
    # l = len(raw_data)
    # f = wave.open(curr_path + filename + ".wav", 'wb')
    # f.setnchannels(1)
    # f.setsampwidth(2)
    # f.setframerate(16000)
    # f.setnframes(l)
    # f.writeframes(raw_data)
    # f.close()
    #
    # # 读取生成波形图
    # samplerate, data = wavfile.read(filename + ".wav")
    # times = np.arange(len(data)) / float(samplerate)
    # # print(len(data), samplerate, times)
    #
    # # 可以以寸为单位自定义宽高 frameon=False 为关闭边框
    # fig = plt.figure(figsize=(20, 5), facecolor="white")
    # ax = fig.add_axes([0, 0, 1, 1])
    # ax.axis('off')
    # plt.fill_between(times, data, linewidth='1', color='green')
    # plt.xticks([])
    # plt.yticks([])
    # plt.savefig(filename + '.png', dpi=100, transparent=False, bbox_inches='tight', edgecolor='w')
    # plt.show()
