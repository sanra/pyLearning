#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ()表示元祖
# [] 表示数组
# {} 表示字典
#  参数表中带*号表示 提交元祖或者数组参数   带**表示提交字典参数


def a(s0, *s1, **s2):
    print('name:', s0, 'age:', s1, 'other:', s2)


def person(name, age, *, city, job):
    print(name, age, city, job)


# 区别在*和 *args  有了可以参数 就不需要继续声明命名关键字参数
def person1(name, age, *args, city, job):
    print(name, age, args, city, job)


def person2(name, age, *args, city='Xi`An', job='Programmer'):
    print(name, age, args, city, job)


# 必选参数、默认参数、可变参数、关键字参数和命名关键字参数
def f1(a, b, c=0, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)


def f2(a, b, c=0, *, d, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'd =', d, 'kw =', kw)


if __name__ == '__main__':
    a('小爱同学', [1, 2, 3], className={'city': 'Beijing', 'job': 'Engineer'})
    person('Jack', 24, city='Beijing', job='Engineer')
    person1('Jack', 24, (1, 2, '111', 'jack', 33), city='Beijing', job='Engineer')
    person2('Sanra', 30, [1, 2, 3, 4, 5, 6, 77, 8, 8, 8])
    f1(1, 2, 3, 'a', 'b', x=99)
    f2(1, 2, d=99, ext={'name': 'Sanra', 'age': 30})
