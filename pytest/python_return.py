#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def return_info():
    name = '2222'
    age = 222
    return name, age


if __name__ == '__main__':
    print(return_info())
    print(return_info()[0])
    print(return_info()[1])
