# !/usr/bin/python3
# -*-coding:utf-8-*-
# Author: sanra
# CreatDate: 2020/11/13 15:08
# Description: 解霸分词测试
import jieba

if __name__ == '__main__':
    text = '中新网11月13日电 据国家卫健委网站消息，11月12日0—24时，31个省(自治区、直辖市)和新疆生产建设兵团报告新增确诊病例8例，均为境外输入病例(上海3例，内蒙古1例，河南1例，四川1例，陕西1例，甘肃1例)；无新增死亡病例；无新增疑似病例。'
    text = text.replace(' ', '')
    text = text.replace('，', '')
    text = text.replace('。', '')
    text = text.replace('（', '')
    text = text.replace('）', '')
    text = text.replace('、', '')
    text = text.replace('(', '')
    text = text.replace(')', '')
    text = text.replace('-', '')
    text = text.replace('，', '')
    seq_l_list = jieba.lcut(text)
    seq_list = jieba.cut(text, cut_all=True)
    print(seq_l_list)
    print(list(seq_list))
