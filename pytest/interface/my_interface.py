#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import math
from abc import abstractmethod, ABCMeta


class GraphicRule(metaclass=ABCMeta):
    @abstractmethod
    def area(self):
        """求面积"""
        pass

    @abstractmethod
    def perimeter(self):
        """周长"""
        pass


class Rectangle(GraphicRule):
    """矩形类"""

    def __init__(self, b, h):
        self.b = b
        self.h = h

    def area(self):
        return self.b * self.h

    def perimeter(self):
        return (self.b + self.h) * 2


class Circle(GraphicRule):

    def __init__(self, r):
        self.r = r

    def area(self):
        return math.pi * self.r * self.r

    def perimeter(self):
        return 2 * math.pi * self.r


if __name__ == '__main__':
    rect1 = Rectangle(3, 7)
    print("矩形面积：{}".format(rect1.area()))
    print("矩形周长：{}".format(rect1.perimeter()))
    rect2 = Circle(30)
    print("圆形面积：{}".format(int(rect2.area())))
    print("圆形周长：{}".format(int(rect2.perimeter())))
