def f(x):
    return str(x)


def log(func):
    def wrapper(*args, **kw):
        print('call %s():' % func.__name__)
        return func(*args, **kw)

    return wrapper


@log
def now():
    print('2015-3-25')


if __name__ == '__main__':
    # map 两个参数 一个函数 一个参数表
    r = map(f, [1, 2, 3, 4, 5, 6, 7, 8, 9])
    for x in r:
        print(x)

    # sorted 两个参数 一个判断条件  一个参数表
    p = sorted([36, 5, -12, 9, -21], key=abs)
    for x in p:
        print(x)
    now()
