# !/usr/bin/python3
# -*-coding:utf-8-*-
# Author: sanra
# CreatDate: 2020/11/11 15:55
# Description: 日志测试
import os

from tools.logger import Logger

if __name__ == '__main__':
    root_path = os.path.abspath(os.path.dirname(__file__)).split('shippingSchedule')[0]
    print(root_path)
    name = os.path.basename(__file__).split(".")[0]
    filename = '%s.log' % name
    log = Logger(path=filename)
    log.debug('一个debug信息')
    log.info('一个info信息')
    log.war('一个warning信息')
    log.error('一个error信息')
    log.cri('一个致命critical信息')
