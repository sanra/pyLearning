#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import threading
import time


class Seeker(threading.Thread):
    def __init__(self, condition, name):
        super(Seeker, self).__init__()
        self.cond = condition
        self.name = name

    def run(self):
        time.sleep(0.5)
        self.cond.acquire()
        print('%s：我已经把眼睛蒙上了。' % self.name)
        self.cond.notify()
        self.cond.wait()
        time.sleep(1)
        print('%s：我找到你了哦。' % self.name)
        self.cond.notify()
        self.cond.wait()
        time.sleep(1)
        print('%s：我赢了' % self.name)
        self.cond.release()


class Hider(threading.Thread):
    def __init__(self, condition, name):
        super(Hider, self).__init__()
        self.cond = condition
        self.name = name

    def run(self):
        self.cond.acquire()
        self.cond.wait()
        time.sleep(1)
        print('%s：我已经藏好了，你来找我吧。' % self.name)
        self.cond.notify()
        self.cond.wait()
        time.sleep(1)
        print('%s：被你找到了。' % self.name)
        self.cond.notify()
        self.cond.release()


if __name__ == '__main__':
    cond = threading.Condition()
    seeker = Seeker(cond, '找人的')
    hider = Hider(cond, '藏起来的')
    seeker.start()
    hider.start()
