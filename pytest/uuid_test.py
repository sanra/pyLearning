#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time
import uuid

if __name__ == '__main__':
    name = 'test_name'
    # namespace = 'test_namespace'
    namespace = uuid.NAMESPACE_DNS
    print("--uuid1--")
    for i in range(1, 10):
        time.sleep(0.1)
        print(uuid.uuid1())
    print("--uuid3--")
    for i in range(1, 10):
        time.sleep(0.1)
        print(uuid.uuid3(namespace, name))
    print("--uuid4--")
    for i in range(1, 10):
        time.sleep(0.1)
        print(uuid.uuid4())
    print("--uuid5--")
    for i in range(1, 10):
        time.sleep(0.1)
        print(uuid.uuid5(namespace, name))
