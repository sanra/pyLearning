#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from multiprocessing import Process


def linux_process():
    print('Process (%s) start...' % os.getpid())
    pid = os.forkpty()
    if pid == 0:
        print('I am child process (%s) and my parent is %s.' % (os.getpid(), os.getppid()))
    else:
        print('I (%s) just created a child process (%s).' % (os.getpid(), pid))


def run_proc(name):
    print('运行子进程%s(%s)......' % (name, os.getpid()))


def win_process():
    print('父进程%s' % os.getpid())
    p = Process(target=run_proc, args=('test',))
    print('子进程将开始')
    p.start()
    p.join()
    print('子进程结束')


if __name__ == '__main__':
    win_process()
