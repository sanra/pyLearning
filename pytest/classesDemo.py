#!/usr/bin/env python3
# -*- coding: utf-8 -*-



class Student(object):
    __name = ''
    __score = 0

    def __init__(self):
        print("初始化")

    def set_name(self, name):
        self.__name = name

    def get_name(self):
        return self.__name

    def set_score(self, score):
        self.__score = score

    def get_score(self):
        return self.__score


if __name__ == '__main__':
    bart = Student()
    bart.set_name(name="战三")
    bart.set_score(score=100)
    print(bart.get_name())
    print(bart.get_score())
