#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Student(object):
    __score = 0
    __name__ = ''

    @staticmethod
    def language(self):
        print('这是一个静态方法')

    def __init__(self):
        self.__name__ = '张思'
        self.__score = 500

    @property
    def score(self):
        return self.__score

    @score.setter
    def score(self, value):
        if not isinstance(value, int):
            raise ValueError('score must be an integer!')
        if value < 0 or value > 100:
            raise ValueError('score must between 0 ~ 100!')
        self.__score = value

    def __str__(self):
        return 'Student object (name: %s)' % self.__score


if __name__ == '__main__':
    s = Student()
    s.__name__ = '22222'
    s.score = 66
    print(s.__name__)
    print(s.score)
    print(s.__str__())
