#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json

if __name__ == '__main__':
    data = [{'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}]
    data2 = json.dumps(data)
    data2 = json.dumps({'a': 'Runoob', 'b': 7}, sort_keys=True, indent=4, separators=(',', ': '))
    print(data2)
    print(type(data2))

    jsonData = '{"a":1,"b":2,"c":3,"d":4,"e":5,"f": { "g":"x" , "h": 322}}'
    text = json.loads(jsonData)
    print(text)
    print(type(text))
    print(text['a'])
    print(text['f']['h'])
