#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# 类关系-继承
# 抽象类和抽象方法
import abc


class Animal(object):
    name = ''

    @abc.abstractmethod
    def run(self):
        pass

    @abc.abstractmethod
    def get_animal_name(self):
        pass


class Dog(Animal):

    def __init__(self):
        self.name = '旺财'

    def get_animal_name(self):
        return "二哈"

    def run(self):
        print('狗在叫唤')


class Cat(Animal):
    pass


if __name__ == '__main__':
    dog = Dog()
    dog.run()
    print(dog.get_animal_name())
    print(isinstance(dog, Animal))
    print(type(dog))
    # 列举类对象所有可用方法
    print(dir(dog))
    # 判断是否存在类属性
    print(hasattr(dog, 'age'))
    print(hasattr(dog, 'name'))
    print(dog.name)
    # 类属性操作
    print(getattr(dog, 'gender', '男'))
    dog.gender = '女'
    print(getattr(dog, 'gender', '男'))
    del dog.gender
    print(getattr(dog, 'gender', '男'))
