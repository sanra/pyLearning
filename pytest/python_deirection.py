#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os

if __name__ == '__main__':
    print(os.name)
    print(os.environ.get('PATH'))
    print(os.path.abspath('.'))
    print(os.path.split('/Users/michael/testdir/file.txt'))
    print(os.path.splitext('/path/to/file.txt'))
