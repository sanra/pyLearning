#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Student(object):
    __slots__ = ('name', 'age')  # 用tuple定义允许绑定的属性名称


# __slots__定义的属性仅对当前类实例起作用，对继承的子类是不起作用的：
class GraduateStudent(Student):
    pass


if __name__ == '__main__':
    s = Student()
    s.name = '11111'
    s.age = 2222
    print(s.name)
    print(s.age)

    g = GraduateStudent()
    g.score = 999
    print(g.score)
