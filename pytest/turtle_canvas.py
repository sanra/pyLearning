#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from turtle import *


# turtle 安装需要走本地安装，
# setup.py里面的40行有个bug，
# except ValueError, ve: 需要加个括号改成except (ValueError, ve):

def draw_star(x, y):
    pu()
    goto(x, y)
    pd()
    # set heading: 0
    seth(0)
    for i in range(5):
        fd(40)
        rt(144)


if __name__ == '__main__':
    for x in range(0, 250, 50):
        draw_star(x, 0)
    done()
