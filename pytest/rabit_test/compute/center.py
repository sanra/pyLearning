# !/usr/bin/python3
# -*-coding:utf-8-*-
# Author: sanra
# CreatDate: 2021/1/7 10:48
# Description:
import pika


# 定义接收到返回消息的处理方法
def on_response(self, ch, method, props, body):
    print("这里执行了吗？")
    response = body
    print(body.decode())


if __name__ == '__main__':
    print(" [x] Requesting increase(30)")

    credentials = pika.PlainCredentials('guest', 'guest')
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host = '127.0.0.1', port = 5672, virtual_host = '/', credentials = credentials))
    channel = connection.channel()
    # 定义接收返回消息的队列
    result = channel.queue_declare(queue = 'compute_queue', durable = False)
    callback_queue = result.method.queue
    response = None
    channel.basic_consume(on_message_callback = on_response,
                          auto_ack = True,
                          queue = callback_queue)
    # 发送计算请求，并声明返回队列
    channel.basic_publish(exchange = 'compute_exchange',
                          routing_key = 'compute',
                          properties = pika.BasicProperties(reply_to = callback_queue),
                          body = str(30))
    connection.process_data_events()

    # print(" [.] Got %r" % response)
