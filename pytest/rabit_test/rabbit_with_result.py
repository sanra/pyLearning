# !/usr/bin/python3
# -*-coding:utf-8-*-
# Author: sanra
# CreatDate: 2021/1/6 18:13
# Description:


# 自定义线程类，继承threading.Thread
import threading
import uuid

import pika


# class MyThread(threading.Thread):
#     def __init__(self, func, num):
#         super(MyThread, self).__init__()
#         self.func = func
#         self.num = num
#
#     def run(self):
#         print(" [x] Requesting increase(%d)" % self.num)
#         response = self.func(self.num)
#         print(" [.] increase(%d)=%d" % (self.num, response))


# 控制中心类
class Center(object):
    def __init__(self):
        credentials = pika.PlainCredentials('guest', 'guest')  # mq用户名和密码
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host = 'localhost', port = 5672, virtual_host = '/', credentials = credentials))
        self.channel = self.connection.channel()

        # 定义接收返回消息的队列
        result = self.channel.queue_declare(exclusive = True, queue = 'hello.queue1', durable = False, passive = True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(on_message_callback = self.on_response,
                                   auto_ack = False,
                                   queue = self.callback_queue)

        # 返回的结果都会存储在该字典里
        self.response = {}

    # 定义接收到返回消息的处理方法
    def on_response(self, ch, method, props, body):
        self.response[props.correlation_id] = body

    def request(self, n):
        corr_id = str(uuid.uuid4())
        self.response[corr_id] = None

        # 发送计算请求，并设定返回队列和correlation_id
        self.channel.basic_publish(exchange = 'topicExchange',
                                   routing_key = 'key.1',
                                   properties = pika.BasicProperties(
                                       reply_to = self.callback_queue,
                                       correlation_id = corr_id,
                                   ),
                                   body = str(n))
        # 接收返回的数据
        while self.response[corr_id] is None:
            self.connection.process_data_events()
        return int(self.response[corr_id])


# center = Center()
# # 发起5次计算请求
# nums = [10, 20, 30, 40, 50]
# threads = []
# for num in nums:
#     threads.append(MyThread(center.request, num))
# for thread in threads:
#     thread.start()
# for thread in threads:
#     thread.join()

if __name__ == '__main__':
    center = Center()
    result = center.request(1)
    print(result)
