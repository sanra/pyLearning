#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pika


def callback(ch, method, properties, body):
    response = body.decode()
    print(response)
    print(properties)
    if properties.reply_to is None:
        properties.reply_to = ''
    # 将计算结果发送回控制中心
    ch.basic_publish(exchange='',
                     routing_key=properties.reply_to,
                     properties=pika.BasicProperties(correlation_id=properties.correlation_id),
                     body=str(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)
    print('返回数据了')


if __name__ == '__main__':
    credentials = pika.PlainCredentials('guest', 'guest')  # mq用户名和密码
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='127.0.0.1', port=5672, virtual_host='/', credentials=credentials))
    channel = connection.channel()
    # 申明消息队列，消息在这个队列传递，如果不存在，则创建队列
    channel.queue_declare(queue='rabbit-test', durable=False)
    # 告诉rabbitMq，用callback来接收消息
    channel.basic_consume(queue='rabbit-test', on_message_callback=callback)
    print('消费者就绪......')
    # 开始接收信息，并进入阻塞状态，队列里有信息才会调用callback进行处理
    channel.start_consuming()
